package com.company.Bean.DAO;

import java.util.List;
import com.company.Bean.Family;
import com.company.Bean.Pet;
import com.company.Bean.Human;

public interface FamilyDAO {

    Family getFamilyByIndex(int index);

    List getAllFamilies();

    void displayAllFamilies();

    void getFamiliesBiggerThan(int number);

    void getFamiliesLessThan(int number);

    void createNewFamily(Human human, Human human2);

    boolean deleteFamily(int index);

    boolean deleteFamily(Family family);

    boolean saveFamily(List<Family> family);

    public boolean deleteFamilyByIndex(int index);

    public void bornChild(Family family, String name);

    public long count();

    public void deleteAllChildrenOlderThen(int child);

    Family getFamilyById(int number) ;

    public List getPets(int number);

    public void addPet(int index, Pet pet);

}
