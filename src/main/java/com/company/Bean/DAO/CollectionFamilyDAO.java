package com.company.Bean.DAO;

import java.util.ArrayList;
import java.util.List;

import com.company.Bean.Family;
import com.company.Bean.Human;
import com.company.Bean.Pet;

public class CollectionFamilyDAO implements FamilyDAO {

    private List<Family> famData = new ArrayList<>();
    @Override
    public List getAllFamilies() {

        ArrayList<Family> Families = new ArrayList<>();

        for (Family f : famData) {
            Families.add(f);
        }
        ;
        return Families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return famData.get(index);
    }
    @Override
    public void displayAllFamilies() {
        ArrayList <Family> families = new ArrayList<>();
        for (Family f : famData) {
            families.add(f);
            System.out.println(f);
        }
    }

    @Override
    public void getFamiliesBiggerThan(int numberOfMembers) {
        ArrayList<Family> families = new ArrayList<>();
        for (Family f : families) {
            if (f.countFamily() > numberOfMembers) {
                families.add(f);
            }

        }
        System.out.println(families);
    }

    @Override
    public void getFamiliesLessThan(int numberOfMembers) {
        ArrayList<Family> families = new ArrayList<>();
        for (Family f : families) {
            if (f.countFamily() < numberOfMembers) {
                families.add(f);
            }

        }
        System.out.println(families);
    }
    //by index

    @Override
    public boolean deleteFamily(int index) {
        famData.remove(index);
        return true;
    }

    @Override
    public boolean deleteFamily(Family family) {

        famData.remove(family);
        return true;

    }

    @Override
    public boolean saveFamily(List<Family> family) {

        this.famData = family;
        return true;

    }

    @Override
    public void createNewFamily(Human mother, Human father) {

        Family family = new Family(mother, father);

        famData.add(family);

    }
    @Override
    public Family getFamilyById(int number) {
        return famData.get(number);
    }

    @Override
    public long count() {
        return famData.stream().count();
    }

    @Override
    public void deleteAllChildrenOlderThen(int child) {


    }

    @Override
    public boolean deleteFamilyByIndex(int index) {

        famData.remove(index);
        return true;

    }

    @Override
    public void bornChild(Family family, String name) {


    }

    @Override
    public List getPets(int number) {

        ArrayList<Pet> pets = new ArrayList<>();
        pets.add(famData.get(number).getPet());

        return pets;

    }

    @Override
    public void addPet(int index, Pet pet) {

        famData.get(index).setPet(pet);


    }

}
