package com.company.Bean.Controller;

import com.company.Bean.DAO.FamilyDAO;
import com.company.Bean.Family;
import com.company.Bean.Human;
import com.company.Bean.Pet;
import com.company.Bean.Service.FamilyService;

import java.util.List;

public class FamilyController implements FamilyDAO {

    FamilyService familyService=new FamilyService();


    @Override
    public List getAllFamilies() {
        return familyService.getAllFamilies();
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return familyService.getFamilyByIndex(index);
    }



    @Override
    public void displayAllFamilies() {
         familyService.displayAllFamilies();
    }

    @Override
    public void getFamiliesBiggerThan(int number) {
          familyService.getFamiliesBiggerThan(number);
    }

    @Override
    public void getFamiliesLessThan(int number) {
          familyService.getFamiliesLessThan(number);
    }

    @Override
    public void createNewFamily(Human human, Human human2) {
          familyService.createNewFamily(human,human2);

    }

    @Override
    public boolean deleteFamily(int index) {
        return familyService.deleteFamily(index );

    }

    @Override
    public long count() {
          return familyService.count();
    }

    @Override
    public void deleteAllChildrenOlderThen(int child) {

    }

    @Override
    public boolean deleteFamily(Family family) {
                 return familyService.deleteFamily(family );
     }

    @Override
    public boolean saveFamily(List<Family> family) {
        return familyService.saveFamily(family );
    }

    @Override
    public boolean deleteFamilyByIndex(int index) {
          return familyService.deleteFamilyByIndex(index);
    }

    @Override
    public void bornChild(Family family, String name) {
          familyService.bornChild(family,name);
    }

    @Override
    public Family getFamilyById(int number) {
         return familyService.getFamilyById(number);
    }

    @Override
    public List getPets(int number) {
        return familyService.getPets(number);
    }

    @Override
    public void addPet(int index, Pet pet) {
          familyService.addPet(index,pet);

    }
}
